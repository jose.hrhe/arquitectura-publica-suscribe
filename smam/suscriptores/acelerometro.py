#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: acelerometro.py
# Capitulo: 3 Estilo Publica-Subscribe
# Autor(es): Manuel Sánchez, José Hernández, Viridiana Murillo & Diego López.
# Version: 2.0.1 Marzo 2020
# Descripción:
#
#   Ésta clase define el rol de la alrma, es decir, genera comprabaciones de tiempo.
#
#   Las características de ésta clase son las siguientes:
#
#                                            acelerometro.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |      Acelerometro     |  - Permite generar      |         Ninguna        |
#           |                       |    procesos con las     |                        |
#           |                       |    posiciones de un     |                        |
#           |                       |    dispositivo.         |                        |
#           +-----------------------+-------------------------+------------------------+

class Acelerometro:
    
    # Evalua el posible cambio de aceleración en un dispositivo.
    # - x: Eje 1
    # - y: Eje 2
    # - z: Eje 3
    # - Xold: Valor previo de x
    # - Yold: Valor previo de y
    # - Zold: Valor previo de z
    # - Xmax: Maxima variación de x
    # - Ymax: Maxima variación de y
    # - Zmax: Maxima variación de z
    def evaluate_fall(self,x,y,z,Xold,Yold,Zold,Xmax,Ymax,Zmax):
        Xres = abs(x - Xold)
        Yres = abs(y - Yold)
        Zres = abs(z - Zold)

        if (Xres > Xmax or
            Yres > Ymax or
            Zres > Zmax):
            return True
        else:
            Xold  = x
            Yold = y
            Zold = z
        return False