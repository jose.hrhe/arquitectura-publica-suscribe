#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: alarma.py
# Capitulo: 3 Estilo Publica-Subscribe
# Autor(es): Manuel Sánchez, José Hernández, Viridiana Murillo & Diego López.
# Version: 2.0.1 Marzo 2020
# Descripción:
#
#   Ésta clase define el rol de la alrma, es decir, genera comprabaciones de tiempo.
#
#   Las características de ésta clase son las siguientes:
#
#                                            alarma.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |        Alarma         |  - Permite generar      |         Ninguna        |
#           |                       |    procesos con el      |                        |
#           |                       |    tiempo.              |                        |
#           +-----------------------+-------------------------+------------------------+

import time

class Alarma:

    # Verifica si la hora actual es igual a la del horario del medicamento
    # - horario: Es la hora de toma del medicamento. 
    def check_alarm(self,horario):
        hora_actual = int(time.time()) % 86400 # Hora actual en segundos
        return horario == hora_actual