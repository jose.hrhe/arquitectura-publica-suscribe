#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: procesador_de_tiempo.py
# Capitulo: 3 Estilo Publica-Subscribe
# Autor(es): Manuel Sánchez, José Hernández, Viridiana Murillo & Diego López.
# Version: 2.0.1 Marzo 2020
# Descripción:
#
#   Esta clase define el rol de un suscriptor, es decir, es un componente que recibe mensajes.
#
#   Las características de ésta clase son las siguientes:
#
#                                     procesador_de_tiempo.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |                       |                         |  - Se suscribe a los   |
#           |                       |                         |    eventos generados   |
#           |                       |  - Procesar valores     |    por el wearable     |
#           |     Procesador de     |    extremos de la       |    Xiaomi My Band.     |
#           |        Tiempo         |    presión arterial.    |  - Define el valor     |
#           |                       |                         |    del tiempo.         | 
#           |                       |                         |  - Notifica al monitor |
#           |                       |                         |    cuando la alarma    |
#           |                       |                         |    tiene que activarse |
#           +-----------------------+-------------------------+------------------------+
#
#   A continuación se describen los métodos que se implementaron en ésta clase:
#
#                                               Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |                        |                          |  - Recibe los signos  |
#           |       consume()        |          Ninguno         |    vitales vitales    |
#           |                        |                          |    desde el distribui-|
#           |                        |                          |    dor de mensajes.   |
#           +------------------------+--------------------------+-----------------------+
#           |                        |  - ch: propio de Rabbit. |  - Procesa y detecta  |
#           |                        |  - method: propio de     |    valores extremos de|
#           |                        |     Rabbit.              |    la presión         |
#           |       callback()       |  - properties: propio de |    arterial.          |
#           |                        |     Rabbit.              |                       |
#           |                        |  - body: mensaje recibi- |                       |
#           |                        |     do.                  |                       |
#           +------------------------+--------------------------+-----------------------+
#           |    string_to_json()    |  - string: texto a con-  |  - Convierte un string|
#           |                        |     vertir en JSON.      |    en un objeto JSON. |
#           +------------------------+--------------------------+-----------------------+
#
#
#           Nota: "propio de Rabbit" implica que se utilizan de manera interna para realizar
#            de manera correcta la recepcion de datos, para éste ejemplo no shubo necesidad
#            de utilizarlos y para evitar la sobrecarga de información se han omitido sus
#            detalles. Para más información acerca del funcionamiento interno de RabbitMQ
#            puedes visitar: https://www.rabbitmq.com/
#
#-------------------------------------------------------------------------

import pika
import sys
sys.path.append('../')
from monitor import Monitor
import time, ast
from alarma import Alarma

class ProcesadorTiempo:

    def consume(self):
        try:
            # Se establece la conexión con el Distribuidor de Mensajes
            connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
            # Se solicita un canal por el cuál se enviarán los datos de los adultos
            channel = connection.channel()
            # Se declara una cola para leer los mensajes enviados por el
            # Publicador
            channel.queue_declare(queue='alarm', durable=True)
            channel.basic_qos(prefetch_count=1)
            channel.basic_consume(on_message_callback=self.callback, queue='alarm')
            channel.start_consuming()  # Se realiza la suscripción en el Distribuidor de Mensajes
        except (KeyboardInterrupt, SystemExit):
            channel.close() # Se cierra la conexion
            sys.exit("Conexion finalizada...")
            time.sleep(1)
            sys.exit("Programa terminado...")


    def callback(self, ch, method, properties, body):
        body = body.decode('utf-8')
        json_message = ast.literal_eval(body)
        medicamentos = json_message['medicamentos']
        monitor = Monitor()
        datetime = json_message['datetime']
        id = json_message['id']
        model = json_message['model']
        alarma = Alarma()
        for indice in range(0, len(medicamentos)):
            # def print_notification_medicine(self,datetime,id,model,medicine,dosis):
            medicamento = medicamentos[str(indice)]
            for horario in medicamento['horarios']:
                if alarma.check_alarm(horario):
                    monitor.print_notification_medicine(datetime, id, model, medicamento['medicamento'], medicamento['dosis'])

        time.sleep(1)
        ch.basic_ack(delivery_tag=method.delivery_tag)
            
    def string_to_json(self, string):
        message = {}
        string = string.decode('utf-8')
        string = string.replace('{', '')
        string = string.replace('}', '')
        values = string.split(', ')
        for x in values:
            v = x.split(': ')
            message[v[0].replace('\'', '')] = v[1].replace('\'', '')
        return message
    
if __name__ == '__main__':
    p_tiempo = ProcesadorTiempo()
    p_tiempo.consume()